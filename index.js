// ===========ARRAY
let grades = [98.5, 94.3 ,89.2 ,90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fijutsu'];
let mixedArr = [12, 'Asus', null, undefined, true];
let myTasks = ['drink html', 'eat javascript',
				'inhale css', 'bake sass'];
// =========ACCESS
console.log(grades);
console.log(computerBrands[computerBrands.length-1], computerBrands[2]);
console.log("Array before reassignment", myTasks);
myTasks[0] = "HEllo World!";
console.log("AFTER",myTasks);

// =========METHOD
let fruits = ['Apple', 'Orange', 'kiwi', 'dragon fruit'];

console.log('Current Array fruits: ', fruits);
console.log("PUSH: New element placed at the END of array: ", fruits.push('Mango'));

console.log('Current Array fruits: ', fruits);
console.log("POP : remove element at the END of array: ", fruits.pop(), fruits);

console.log('Current Array fruits: ', fruits);
console.log("UNSHIFT: New element/s placed at the FRONT of array: ",fruits.unshift('Mango','cherry'), fruits);

console.log('Current Array fruits: ', fruits);
console.log("SHIFT : remove element at the FRONT of array: ", fruits.shift(), fruits);

console.log('Current Array fruits: ', fruits);
//PARAMETER: arr.splice(startIndex, deletecount, elementToBeAdded)
console.log("SPLICE : remove element/s of the specified index: ", fruits.splice(1,2,'lime','guava'), fruits);
//ref : 'cherry', 'Apple', 'Orange', 'kiwi', 'dragon fruit'
// expected output : 'cherry','Lime','guava', 'kiwi', 'dragon fruit'

console.log('Current Array fruits: ', fruits);
console.log("SORT : arrange element in alphanumeric(ASCII) order: ",fruits.sort());

console.log('Current Array fruits: ', fruits);
console.log("REVERSE :  the orderreverse element in alphanumeric(ASCII) order: ",fruits.reverse());

// ===========NON-MUTATOR METHODS
let countries = ['US', 'PH', 'CAN', 'PH', 'SG', 'TH', 'PH', 'FR', 'DE', 'PH'];

console.log('Current Array countries: ', countries);
console.log("=============INDEXOF(): return index number of first match found in array: 'PH' =  ", countries.indexOf('PH'));

console.log('Current Array countries: ', countries);
console.log("IndexOf(): NO MATCH: 'BR' =  ", countries.indexOf('BR'));

console.log('Current Array countries: ', countries);
console.log("LastIndexOf(searchValue): return index number of LAST match found in array: 'PH' =  ", countries.lastIndexOf('PH'));

console.log('Current Array countries: ', countries);
console.log("LastIndexOf(searchValue, limitIndex): return index number of LAST match found in array: 'PH' =  ", countries.lastIndexOf('PH', 2));

console.log('Current Array countries: ', countries);
console.log("LastIndexOf(startingIndex): slice element from an array and returns a new array (startingIndex = 2) =  ", countries.slice(2));

console.log('Current Array countries: ', countries);
console.log("LastIndexOf(startingIndex, endIndex): slice element from an array and returns a new array (startingIndex = 2, endIndex = 5) =  ", countries.slice(2,5));

console.log('Current Array countries: ', countries);
console.log("toString(): return string from araay separated by coma =  ", countries.toString());

let taskArrayA = ["drink html", "eat javascript"];
let taskArrayB = ["inhale css", "breath sass"];
let taskArrayC = ["get git", "be node"];

console.log(taskArrayA,taskArrayB,taskArrayC);
console.log("a.concat(b): combine 2 array and return combined result =  ", taskArrayA.concat(taskArrayB));
console.log("a.concat(b,c): combine 2 array and return combined result =  ", taskArrayA.concat(taskArrayB, taskArrayC));
console.log("a.concat('smell express', 'throw react'): combine 2 array and return combined result =  ", taskArrayA.concat('smell express', 'throw react'));

let user = ['john','Jane', 'Joe', 'robert']
console.log("join(\"SEPARATOR\"): join elements of array (SEPARATOR '*')=  ", user.join("*"));

//========================================DAY2
let numbers = [1,2,3,4,5];

//================== FOREACH()
// arr.forEach(func(element)): similar to a for loop that iterates on each array element

allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
allTasks.forEach(function(task){console.log(task);});

let filteredTasks = [];
allTasks.forEach(function(task){if(task.length > 10){filteredTasks.push(task);}});
console.log("FOREACH(): filtered task", filteredTasks);

let filteredNumbers = [];
numbers.forEach(function(number){if(number<3){filteredNumbers.push(number);}});
console.log("FOREACH([1,2,3,4,5]) result", filteredNumbers)

//================MAP()
// iterate on each elemenet and returns a new array with different values depending on the result of the function's operation.

let numberMap = numbers.map(function(number){return number*number;});
console.log('MAP() result:', numberMap);

//==============EVERY()
//check if all elements in an array meet the given condition; Bool result
let allValid = numbers.every(function(number){return(number<3);})
console.log("EVERY() result", allValid);

//=============SOME()
//check if atleast one element in the array meets the given condition; BOOL result
let someValid = numbers.some(function(number){return(number<2)});
console.log("SOME() result", someValid);

//============FILTER()
//returns new array that containd element which meets the given condition
let filterValid = numbers.filter(function(number){return(number<3)});
console.log("FILTER() result", filterValid);
let nothingFound=numbers.filter(function(number){return(number === 0);});
console.log("FILTER(EMPTY) result", nothingFound);

//=========INCLUDES()
let products = ["Mouse", "Keyboard", "Laptop", "MOnitor"];
let filteredProducts = products.filter(function(product){return product.includes('a')});
console.log("INCLUDES() result", filteredProducts);

//=========REDUCE()
//evaluates elements from left to right and returns/reduces the array into single value.
let iteration = 0;
let reducedArray = numbers.reduce(function(x,y){
	console.warn("current:" + ++iteration);
	console.log("accumulator:" + x);
	console.log("current value:" + y);
	return x+y;
});
console.log("REDUCE(number) result", reducedArray);

let listString = ["hello","again","world"];
let reduceJoin = listString.reduce(function(x,y) {
	return x+"-"+y;
});
console.log("REDUCE(string) result", reduceJoin);

//===================Multidimentional Array
let twoDim = [[2,4,6],[8,10,10]]; //2rx3c
console.table(twoDim);
console.log(twoDim[0][1], twoDim[1][0]);

//3X2
let threeTwoDim = [['gie','loe'],['ren','ken'],['oli','rob']];
console.table(threeTwoDim);

function showName(x){
	console.log("hello - " + x);
}







